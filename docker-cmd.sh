#!/bin/sh

# Trap SIGTERM and SIGINT
trap '/stop-container.sh; exit $?' SIGTERM SIGINT

# Start the container
/start-container.sh

# Wait for signal
while true; do :; done
