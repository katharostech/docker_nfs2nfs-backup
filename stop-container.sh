#!/bin/sh

# Kill the cron daemon
kill -TERM $(cat /var/run/crond.pid)
