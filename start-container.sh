#!/bin/sh

cron_log_file=/var/log/crond.log

if [ -n "$SOURCE_OPTIONS" ]; then
    source_options_string="-o $SOURCE_OPTIONS"
fi

if [ -n "$TARGET_OPTIONS" ]; then
    target_options_string="-o $TARGET_OPTIONS"
fi

# Mount the nfs devices we will be backing up to and from
mkdir /mnt/source-nfs
mount -t nfs4 -r $source_options_string ${SOURCE_SERVER}:$SOURCE_PATH /mnt/source-nfs

mkdir /mnt/target-nfs
mount -t nfs4 $target_options_string ${TARGET_SERVER}:$TARGET_PATH /mnt/target-nfs

# Schedule the run of the backups
echo "${CRON_SCHEDULE} /backup-nfs.sh" >> /etc/crontabs/root

# Start the cron daemon with the most verbose logs and a log file
crond -l 8 -L $cron_log_file

# Tail the cron log after it exists
while [ ! -f $cron_log_file ]; do :; done
tail -f $cron_log_file
