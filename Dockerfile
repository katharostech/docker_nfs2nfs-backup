FROM alpine

# Set environment variable defaults
ENV CRON_SCHEDULE='0 0 * * *' \
    SOURCE_OPTIONS='' \
    TARGET_OPTIONS='' \
    SOURCE_PATH='' \
    TARGET_PATH='' \
    SOURCE_SERVER='' \
    TARGET_SERVER=''

# Update operating system
RUN apk upgrade --no-cache

# Install tools for mounting nfs
RUN apk add --no-cache nfs-utils bash

# Copy in the nfs backup script
COPY backup-nfs.sh /backup-nfs.sh
RUN chmod 744 /backup-nfs.sh

# Copy in our start and stop scripts
COPY start-container.sh /start-container.sh
COPY stop-container.sh /stop-container.sh
RUN chmod 744 /start-container.sh
RUN chmod 744 /stop-container.sh

# Copy in our docker-cmd.sh
COPY docker-cmd.sh /docker-cmd.sh
RUN chmod 744 docker-cmd.sh

# Set the Docker command
CMD ["/docker-cmd.sh"]
