#!/bin/bash

source_dir=/mnt/source-nfs
target_dir=/mnt/target-nfs

# Increment backup numbers
# For every backup nubmer
for ((i=$(($BACKUP_RETENTION-1)); i > 0; i--)); do
    # For every backup of that number
    for file in $(ls ${target_dir}/*.tgz.$i 2> /dev/null); do
        # Increment backup number
        file_name=$(echo $file | awk -F . '{print $1}')
        mv $file $file_name.tgz.$(($i+1))
    done
done

# Create new backups
cd ${source_dir}
for dir in $(ls); do
    echo "Starting tar of '$dir'"
    tar -czf ${target_dir}/${dir}.tgz.1 ${dir} &
done
